/**
 * MainActivity.java
 * Jeff Ondich, 3 April 2015
 *
 * BackgroundThreadDemo illustrates the creation of a simple worker thread
 * to do a time-consuming computation and report progress and completion
 * to the main (UI) thread.
 *
 * The background task in this case is to take a given positive integer N
 * and determine (1) the number of primes < N, and (2) the largest prime < N.
 */

package com.jeffondich.backgroundthreaddemo;

import android.content.Context;
import android.os.Handler;
import android.os.Message;
import android.support.v7.app.ActionBarActivity;
import android.os.Bundle;
import android.view.Menu;
import android.view.MenuItem;
import android.view.View;
import android.view.inputmethod.InputMethodManager;
import android.widget.Button;
import android.widget.EditText;
import android.widget.ProgressBar;
import android.widget.TextView;


public class MainActivity extends ActionBarActivity implements View.OnClickListener {
    private final static int MESSAGE_TYPE_PROGRESS = 1;
    private final static int MESSAGE_TYPE_DONE = 2;

    private int topInteger;
    private PrimeHandler handler;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_main);

        // Set up the button
        Button button = (Button)this.findViewById(R.id.go_button);
        button.setOnClickListener(this);

        // Create the handler to be used by the background thread.
        this.handler = new PrimeHandler();
    }


    @Override
    public boolean onCreateOptionsMenu(Menu menu) {
        // Inflate the menu; this adds items to the action bar if it is present.
        getMenuInflater().inflate(R.menu.menu_main, menu);
        return true;
    }

    @Override
    public boolean onOptionsItemSelected(MenuItem item) {
        // Handle action bar item clicks here. The action bar will
        // automatically handle clicks on the Home/Up button, so long
        // as you specify a parent activity in AndroidManifest.xml.
        int id = item.getItemId();

        //noinspection SimplifiableIfStatement
        if (id == R.id.action_settings) {
            return true;
        }

        return super.onOptionsItemSelected(item);
    }

    @Override
    public void onClick(View v) {
        // Drop the keyboard and get N
        this.hideKeyboard();
        EditText integerTextField = (EditText)this.findViewById(R.id.integer_edit_text);
        String integerString = integerTextField.getText().toString();
        this.topInteger = Integer.parseInt(integerString);

        // Initialize the progress bar
        ProgressBar progressBar = (ProgressBar)this.findViewById(R.id.progress_bar);
        progressBar.setMax(this.topInteger);
        progressBar.setProgress(0);
        TextView resultTextView = (TextView)this.findViewById(R.id.result_text_view);
        resultTextView.setText(R.string.computing_caption);

        // Create the worker thread and start it up.
        Thread thread = new Thread(new PrimeRunnable());
        thread.setDaemon(true);
        thread.start();
    }

    public void hideKeyboard() {
        EditText myEditText = (EditText)this.findViewById(R.id.integer_edit_text);
        InputMethodManager imm = (InputMethodManager)getSystemService(Context.INPUT_METHOD_SERVICE);
        imm.hideSoftInputFromWindow(myEditText.getWindowToken(), 0);
    }

    private class PrimeHandler extends Handler {
        @Override
        public void handleMessage(Message message) {
            ProgressBar progressBar = (ProgressBar)findViewById(R.id.progress_bar);

            switch (message.what) {
                case MainActivity.MESSAGE_TYPE_PROGRESS:
                    progressBar.setProgress(message.arg1);
                    break;

                case MainActivity.MESSAGE_TYPE_DONE:
                    progressBar.setProgress(topInteger);
                    int primeCount = message.arg1;
                    int maxPrime = message.arg2;
                    String resultMessage = String.format("Below %d:\n  Biggest prime: %d\n  # of primes: %d",
                                                            topInteger, maxPrime, primeCount);
                    TextView resultTextView = (TextView)findViewById(R.id.result_text_view);
                    resultTextView.setText(resultMessage);
                    break;
            }
        }
    }

    private class PrimeRunnable implements Runnable {
        public void run() {
            int primeCount = 0;
            int maxPrime = 0;
            for (int k = 0; k < topInteger; ++k) {
                if (this.isPrime(k)) {
                    ++primeCount;
                    maxPrime = k;
                }

                // Send a progress report
                if (k % 100 == 0) {
                    Message message = new Message();
                    message.arg1 = k; // This is how much progress we've made
                    message.what = MainActivity.MESSAGE_TYPE_PROGRESS;
                    handler.sendMessage(message);
                }
            }

            Message message = new Message();
            message.arg1 = primeCount;
            message.arg2 = maxPrime;
            message.what = MainActivity.MESSAGE_TYPE_DONE;
            handler.sendMessage(message);
        }

        // Not quite the maximally stupid primality checker, but close.
        // This is intended to be slow, so we can easily demonstrate a
        // time-consuming background thread.
        private boolean isPrime(int n) {
            int squareRoot = (int)Math.sqrt((double)n);
            if (n % 2 == 0) {
                return false;
            }

            for (int k = 3; k <= squareRoot; k += 2) {
                if (n % k == 0) {
                    return false;
                }
            }

            return true;
        }
    }
}
