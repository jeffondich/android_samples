package com.jeffondich.downloaddemo;

import android.content.Context;
import android.net.Uri;
import android.os.Handler;
import android.os.Message;
import android.support.v7.app.ActionBarActivity;
import android.os.Bundle;
import android.util.Log;
import android.view.Menu;
import android.view.MenuItem;
import android.view.View;
import android.view.inputmethod.InputMethodManager;
import android.widget.Button;
import android.widget.EditText;
import android.widget.ImageView;
import android.widget.ProgressBar;

import java.io.File;
import java.util.ArrayList;
import java.util.HashMap;


public class MainActivity extends ActionBarActivity implements View.OnClickListener {
    private final String IMAGES_DIRECTORY_NAME = "images";
    private final int DOWNLOAD_TIMEOUT_SECONDS = 10;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_main);

        Button button = (Button)this.findViewById(R.id.download_button);
        button.setOnClickListener(this);
    }


    @Override
    public boolean onCreateOptionsMenu(Menu menu) {
        // Inflate the menu; this adds items to the action bar if it is present.
        getMenuInflater().inflate(R.menu.menu_main, menu);
        return true;
    }

    @Override
    public boolean onOptionsItemSelected(MenuItem item) {
        // Handle action bar item clicks here. The action bar will
        // automatically handle clicks on the Home/Up button, so long
        // as you specify a parent activity in AndroidManifest.xml.
        int id = item.getItemId();

        //noinspection SimplifiableIfStatement
        if (id == R.id.action_settings) {
            return true;
        }

        return super.onOptionsItemSelected(item);
    }

    @Override
    public void onClick(View v) {
        EditText fileNameTextField = (EditText)this.findViewById(R.id.file_name_edit_text);
        String fileName = fileNameTextField.getText().toString();
        FileDownloadHandler handler = new FileDownloadHandler();
        this.downloadFiles(fileName, handler);
    }

    public void downloadFiles(String fileName, Handler downloadHandler) {
        File imagesDirectory = new File(this.getApplicationContext().getFilesDir(), IMAGES_DIRECTORY_NAME);
        if (!imagesDirectory.exists()) {
            if (!imagesDirectory.mkdir()) {
                return;
            }
        }

        this.hideKeyboard();

        File destination = new File(imagesDirectory, fileName);
        String sourceURL = "http://cs.carleton.edu/faculty/jondich/pics/" + fileName;
        HashMap<String, String> requestHeaders = new HashMap<String, String>();
        requestHeaders.put("User-Agent", "Ondich-Android-Downloader-Sample");

        Downloader downloader = new Downloader();
        downloader.addFileToDownload(sourceURL, destination);
        downloader.download(DOWNLOAD_TIMEOUT_SECONDS, requestHeaders, downloadHandler);
    }

    public void hideKeyboard() {
        EditText myEditText = (EditText)this.findViewById(R.id.file_name_edit_text);
        InputMethodManager imm = (InputMethodManager)getSystemService(Context.INPUT_METHOD_SERVICE);
        imm.hideSoftInputFromWindow(myEditText.getWindowToken(), 0);
    }

    private class FileDownloadHandler extends Handler {
        @Override
        public void handleMessage(Message message) {
            File file;
            String s;
            ProgressBar progressBar;

            switch (message.what) {
                case Downloader.HANDLE_STARTING_ONE_FILE:
                    progressBar = (ProgressBar)findViewById(R.id.progress_bar);
                    progressBar.setMax(100);
                    progressBar.setProgress(0);
                    break;

                case Downloader.HANDLE_PROGRESS:
                    progressBar = (ProgressBar)findViewById(R.id.progress_bar);
                    int progressPercentage = message.arg2 == 0 ? 0 : (100 * message.arg1) / message.arg2;
                    progressBar.setProgress(progressPercentage);
                    break;

                case Downloader.HANDLE_ALL_FILES_SUCCESS:
                    ArrayList<Downloader.Downloadable> downloadables = (ArrayList<Downloader.Downloadable>) message.obj;
                    if (downloadables != null && downloadables.size() > 0) {
                        ImageView imageView = (ImageView)findViewById(R.id.image_view);
                        Downloader.Downloadable downloadable = downloadables.get(0);
                        Uri uri = Uri.fromFile(downloadable.destinationFile);
                        imageView.setImageURI(uri);
                    }
                    break;

                case Downloader.HANDLE_TIMEOUT:
                    file = (File)message.obj;
                    s = (file != null ? file.getName() : "??");
                    Log.d("DOWNLOAD_DEMO", "Download timed out for " + s);
                    break;

                case Downloader.HANDLE_ERROR:
                    Exception e = (Exception)message.obj;
                    Log.d("DOWNLOAD_DEMO", "Downloading error: " + e);
                    break;
            }
            super.handleMessage(message);
        }

    }
}
