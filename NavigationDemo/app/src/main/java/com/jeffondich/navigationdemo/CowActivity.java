package com.jeffondich.navigationdemo;

import android.content.Intent;
import android.support.v7.app.ActionBarActivity;
import android.os.Bundle;
import android.view.Menu;
import android.view.MenuItem;
import android.widget.EditText;


public class CowActivity extends ActionBarActivity {

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_cow);
    }


    @Override
    public boolean onCreateOptionsMenu(Menu menu) {
        // Inflate the menu; this adds items to the action bar if it is present.
        getMenuInflater().inflate(R.menu.menu_cow, menu);
        return true;
    }

    @Override
    public void onBackPressed() {
        this.finishWithCowName();
    }

    @Override
    public boolean onOptionsItemSelected(MenuItem item) {
        // Handle action bar item clicks here. The action bar will
        // automatically handle clicks on the Home/Up button, so long
        // as you specify a parent activity in AndroidManifest.xml.
        int id = item.getItemId();

        //noinspection SimplifiableIfStatement
        if (id == R.id.action_cow) {
            return true;
        } else if (id == android.R.id.home) {
            this.finishWithCowName();
            return true;
        }

        return super.onOptionsItemSelected(item);
    }

    private void finishWithCowName() {
        // Since CowActivity was launched with startActivityForResult, we can communicate
        // information back to the launching activity (in this case, MainActivity) by
        // adding information to an Intent and attaching it to this Activity via the
        // setResult method before calling finish(). The Intent acts roughly like a
        // Dictionary/Hash table, with String keys and arbitrary objects as values.
        // The MainActivity can use the same key (MainActivity.COW_NAME_KEY) to retrieve
        // the information we're sending back (in this case, the string entered by the
        // user into our cow name EditText widget.
        Intent returnIntent = new Intent();
        EditText cowNameEditText = (EditText)this.findViewById(R.id.cow_name);
        returnIntent.putExtra(MainActivity.COW_NAME_KEY, cowNameEditText.getText().toString());
        this.setResult(RESULT_OK, returnIntent);
        this.finish();
        overridePendingTransition(R.anim.slide_in_from_left, R.anim.slide_out_to_right);
    }
}
