package com.jeffondich.navigationdemo;

import android.content.Intent;
import android.support.v7.app.ActionBarActivity;
import android.os.Bundle;
import android.view.Menu;
import android.view.MenuItem;
import android.widget.Toast;


public class MainActivity extends ActionBarActivity {

    // These constants are used to facilitate communication between the MainActivity
    // and the CowActivity. See the startActivityForResult call below.
    public static final int COW_REQUEST_CODE = 1;
    public static final String COW_NAME_KEY = "cow_name";

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_main);
    }


    @Override
    public boolean onCreateOptionsMenu(Menu menu) {
        // Inflate the menu; this adds items to the action bar if it is present.
        getMenuInflater().inflate(R.menu.menu_main, menu);
        return true;
    }

    @Override
    public boolean onOptionsItemSelected(MenuItem item) {
        int id = item.getItemId();

        if (id == R.id.action_moose) {
            // Here's where you launch a new activity. In this case, we don't need anything
            // sent back to MainActivity when the user exits MooseActivity, so we just use the
            // startActivity method.
            Intent intent = new Intent(this, MooseActivity.class);
            startActivity(intent);

            // Since startActivity just queued up the "launch the MooseActivity" operation
            // to be handled later, we still have time to say "animate the transition between
            // this activity and the MooseActivity as follows..." before that happens. This
            // overridePendingTransition call takes care of that.
            overridePendingTransition(R.anim.slide_in_from_right, R.anim.slide_out_to_left);
            return true;

        } else if (id == R.id.action_cow) {
            // In this case, we want to get information back from the CowActivity
            // (specifically, we want the cow's name as entered by the user).
            // The startActivityForResult method helps us do that.
            Intent intent = new Intent(this, CowActivity.class);
            startActivityForResult(intent, MainActivity.COW_REQUEST_CODE);
            overridePendingTransition(R.anim.slide_in_from_right, R.anim.slide_out_to_left);
            return true;
        }

        return super.onOptionsItemSelected(item);
    }

    @Override
    protected void onActivityResult(int requestCode, int resultCode, Intent data) {
        switch (requestCode) {
            case MainActivity.COW_REQUEST_CODE:
                if (resultCode == RESULT_OK) {
                    String cowName = data.getStringExtra(MainActivity.COW_NAME_KEY);
                    String message = "The cow's name is '" + cowName + "'";
                    Toast toast = Toast.makeText(this, message, Toast.LENGTH_SHORT);
                    toast.show();
                }
                break;
        }
    }
}
